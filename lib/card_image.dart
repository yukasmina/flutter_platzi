import 'package:flutter/material.dart';
import 'package:flutter_platzi/floating_action_button_green.dart';

class CardImage extends StatelessWidget {
  String pathImage;
  CardImage(this.pathImage);
  @override
  Widget build(BuildContext context) {
    /**
     * Crear un card para el collage de imagenes
     */
    final card = Container(
      height: 350.0,
      width: 250.0,
      margin: EdgeInsets.only(top: 80.0, left: 20.0),
      decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(pathImage),
          ),
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          shape: BoxShape.rectangle,
          //color de la sobra den la imagen
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                blurRadius: 15.0, //degradado de la sombra
                offset: Offset(0.0, 7.0)) //posicion de la sombra en x e y
          ]),
    );

    return Stack(
      alignment: Alignment(0.9, 1.1),
      children: <Widget>[card, FloatingActionButtonGreen()],
    );
  }
}
