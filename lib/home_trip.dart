import 'package:flutter/material.dart';
import 'package:flutter_platzi/review_list.dart';

import 'description_place.dart';
import 'header_appbar.dart';

class HomeTrip extends StatelessWidget {
  String descriptionSumary =
      "Algo que debemos recordar es que los widgets que usamos como children, pueden definirse bien sea directamente (inline) o bien a través de clases externas (o widgets personalizados) que hemos definido previamente en archivos .dart independientes Para utilizar valores que nos permitan tener contenido dinámico en nuestra interfaz es necesario apoyarnos";
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        //La lista permite que pase por debajo del gradiente
        ListView(
          children: <Widget>[
            DescriptionPlace("San Lucas", 4, descriptionSumary),
            ReviewList()
          ],
        ),
        HeaderAppBar()
      ],
    );
  }
}
