import 'package:flutter/material.dart';
import 'package:flutter_platzi/gradient_perfil.dart';
import 'package:flutter_platzi/menu_perfil.dart';
import 'package:flutter_platzi/review_perfil.dart';

class HeaderPerfil extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final perfil = Container(
      height: 300.0,
      child: ReviewPerfil(
          "assets/img/user1.jpeg", "Angel Minga", "afmingam@unl.edu.ec"),
    );
    return Stack(
      children: <Widget>[GradientePerfil(), perfil],
    );
  }
}
