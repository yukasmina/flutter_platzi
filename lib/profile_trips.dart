import 'package:flutter/material.dart';
import 'package:flutter_platzi/card_image_perfil_list.dart';
import 'package:flutter_platzi/headder_perfil.dart';
import 'package:flutter_platzi/menu_perfil.dart';

class ProfileTrips extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        HeaderPerfil(),
        Positioned(
          child: Icon(
            Icons.settings,
            size: 15,
            color: Colors.black87,
          ),
          right: 20,
          top: 45,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[MenuPerfil()],
        ),
        ListView(
          padding: EdgeInsets.only(top: 230, left: 12, right: 12),
          children: <Widget>[CardImagePerfilList()],
        ),
      ],
    );
  }
}
