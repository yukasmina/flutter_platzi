import 'package:flutter/material.dart';

import 'floating_action_button_green.dart';

class CardImagePerfil extends StatelessWidget {
  String pathImage;
  String title;
  String coment;
  String steps;
  CardImagePerfil(this.pathImage, this.title, this.coment, this.steps);
  @override
  Widget build(BuildContext context) {
    /**
     * Crear un card para el collage de imagenes
     */
    final card = Container(
      height: 250.0,
      width: 350.0,
      margin: EdgeInsets.only(top: 80.0, left: 20.0),
      decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(pathImage),
          ),
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          shape: BoxShape.rectangle,
          //color de la sobra den la imagen
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                blurRadius: 15.0, //degradado de la sombra
                offset: Offset(0.0, 7.0)) //posicion de la sombra en x e y
          ]),
    );
    final titles = Container(
      margin: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
      child: new Text(
        title,
        style: const TextStyle(
            fontFamily: "Lato",
            fontSize: 14.0,
            fontWeight: FontWeight.bold,
            color: Color(0xFF56575a)),
      ),
    );
    final comentar = Container(
      margin: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
      child: new Text(
        coment,
        style: const TextStyle(
            fontFamily: "Lato",
            fontSize: 10.0,
            fontWeight: FontWeight.bold,
            color: Color(0xFF56575a)),
      ),
    );
    final stepes = Container(
      margin: new EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
      child: new Text(
        steps,
        style: const TextStyle(
            fontFamily: "Lato",
            fontSize: 10.0,
            fontWeight: FontWeight.bold,
            color: Color(0xFF56575a)),
      ),
    );
    final carddetalle = Container(
      height: 100.0,
      width: 300.0,
      margin: EdgeInsets.only(top: 80.0, left: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [titles, comentar, stepes],
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          shape: BoxShape.rectangle,
          //color de la sobra den la imagen
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.white,
                blurRadius: 15.0, //degradado de la sombra
                offset: Offset(0.0, 1.0)) //posicion de la sombra en x e y
          ]),
    );
    return Stack(
      alignment: Alignment(0.5, 1.5),
      children: <Widget>[card, carddetalle, FloatingActionButtonGreen()],
    );
  }
}
