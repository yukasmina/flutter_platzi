import 'package:flutter/material.dart';

class ReviewPerfil extends StatelessWidget {
  /*
   * Declaracion de variables
   */
  String pathImagen;
  String name;
  String email;
  /*
   * cracion de constructor
   */
  ReviewPerfil(this.pathImagen, this.name, this.email);
  @override
  Widget build(BuildContext context) {
    /*
     * Nombre del usuario
     */
    final userName = Container(
      margin: EdgeInsets.only(top: 120.0, left: 20.0),
      child: Text(
        name,
        textAlign: TextAlign.left,
        style: TextStyle(fontFamily: "Lato", fontSize: 17.0),
      ),
    );
    /*
     * Informacion del usuario
     */
    final userInfo = Row(children: <Widget>[
      Container(
        margin: EdgeInsets.only(top: 10.0, left: 20.0),
        child: Text(
          email,
          textAlign: TextAlign.left,
          style: TextStyle(
              fontFamily: "Lato", fontSize: 15.0, color: Colors.white),
        ),
      ),
    ]);
    final userDetails = Column(
      crossAxisAlignment: CrossAxisAlignment.start, // se aline a a la izquierda
      children: <Widget>[userName, userInfo],
    );

    final photo = Container(
      margin: EdgeInsets.only(left: 20.0),
      width: 80.0,
      height: 80.0,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            fit: BoxFit.cover, //permte central la imagen
            image: AssetImage(pathImagen),
          )),
    );
    final detailProfile = Padding(
      padding: const EdgeInsets.only(left: 12, top: 85, right: 12),
      child: Row(
        children: <Widget>[
          Container(
            height: 85,
            width: 85,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/img/traveller.jpg"),
                ),
                shape: BoxShape.circle,
                border: Border.all(width: 2, color: Colors.white)),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 12, right: 15),
            child: Text.rich(TextSpan(children: [
              TextSpan(
                  text: "Anita la Huerfanita\n",
                  style: TextStyle(
                      color: Colors.white, fontSize: 16, fontFamily: "Lato")),
              TextSpan(
                  text: "huerfanitanita99@hotmail.com",
                  style: TextStyle(
                      color: Colors.white24, fontSize: 16, fontFamily: "Lato"))
            ])),
          )
        ],
      ),
    );
    return Row(
      children: <Widget>[photo, userDetails],
    );
  }
}
