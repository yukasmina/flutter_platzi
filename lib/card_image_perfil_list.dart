import 'package:flutter/material.dart';
import 'package:flutter_platzi/card_image_perfil.dart';

class CardImagePerfilList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        CardImagePerfil(
            "assets/img/recursos/recurso1.jpeg",
            "Knuckles Mountains",
            "Viene el gran reto, es momento de poner a prueba todo lo que has aprendido",
            "123,123,123"),
        CardImagePerfil(
            "assets/img/recursos/recurso2.jpeg",
            "Knuckles Mountains",
            "Viene el gran reto, es momento de poner a prueba todo lo que has aprendido",
            "123,123,123"),
        CardImagePerfil(
            "assets/img/recursos/recurso3.jpeg",
            "Knuckles Mountains",
            "Viene el gran reto, es momento de poner a prueba todo lo que has aprendido",
            "123,123,123"),
        CardImagePerfil(
            "assets/img/recursos/recurso4.jpeg",
            "Knuckles Mountains",
            "Viene el gran reto, es momento de poner a prueba todo lo que has aprendido",
            "123,123,123"),
        CardImagePerfil(
            "assets/img/recursos/recurso5.jpeg",
            "Knuckles Mountains",
            "Viene el gran reto, es momento de poner a prueba todo lo que has aprendido",
            "123,123,123"),
        CardImagePerfil(
            "assets/img/recursos/recurso5.jpeg",
            "Knuckles Mountains",
            "Viene el gran reto, es momento de poner a prueba todo lo que has aprendido",
            "123,123,123"),
      ],
    );
  }
}
