import 'package:flutter/material.dart';

class MenuPerfil extends StatefulWidget {
  @override
  _MenuPerfilState createState() => _MenuPerfilState();
}

class _MenuPerfilState extends State<MenuPerfil> {
  @override
  Widget build(BuildContext context) {
    Widget CreateIconButton(IconData iconData,
        {bool mini = false, bool active = false}) {
      return Expanded(
        flex: 1,
        child: Container(
          height: mini ? 35 : 55,
          width: mini ? 35 : 55,
          child: Center(
            child: Icon(
              iconData,
              color: Color(0xFF584CD1),
              size: mini ? 20 : 45,
            ),
          ),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: active ? Colors.white : Colors.white54),
        ),
      );
    }

    final widgetMenu = Padding(
      padding: const EdgeInsets.only(top: 200),
      child: Row(
        children: <Widget>[
          CreateIconButton(Icons.bookmark_border, mini: true, active: true),
          CreateIconButton(Icons.card_giftcard, mini: true),
          CreateIconButton(Icons.add, active: true),
          CreateIconButton(Icons.mail_outline, mini: true),
          CreateIconButton(Icons.person, mini: true),
        ],
      ),
    );
    return widgetMenu;
  }
}
