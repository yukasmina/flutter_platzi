import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Review extends StatelessWidget {
  /*
   * Declaracion de variables
   */
  String pathImagen;
  String name;
  String details;
  String comment;
  int stars;

  /*
   * cracion de constructor
   */
  Review(this.pathImagen, this.name, this.details, this.comment, this.stars);

  @override
  Widget build(BuildContext context) {
    /***
     * Estrellas
     */
    final starhalf = Container(
        margin: EdgeInsets.only(
          right: 3.0,
        ),
        child: Icon(
          Icons.star_half,
          color: Color(0xFFf2C611),
        ));
    final starborder = Container(
        margin: EdgeInsets.only(
          right: 3.0,
        ),
        child: Icon(
          Icons.star_border,
          color: Color(0xFFf2C611),
        ));
    final star = Container(
        margin: EdgeInsets.only(
          right: 3.0,
        ),
        child: Icon(
          Icons.star,
          color: Color(0xFFf2C611),
        ));
    /**
     * Nombre del usuario
     */
    final userName = Container(
      margin: EdgeInsets.only(left: 20.0),
      child: Text(
        name,
        textAlign: TextAlign.left,
        style: TextStyle(fontFamily: "Lato", fontSize: 17.0),
      ),
    );
    /**
     * Informacion del usuario
     */
    final userInfo = Row(children: <Widget>[
      Container(
        margin: EdgeInsets.only(left: 20.0),
        child: Text(
          details,
          textAlign: TextAlign.left,
          style: TextStyle(
              fontFamily: "Lato", fontSize: 15.0, color: Color(0xFFa3a5a7)),
        ),
      ),
      Row(
        children: <Widget>[star, star, star, starhalf, starborder],
      )
    ]);
    /**
     * Comentario del usuario
     */
    final userComment = Container(
      margin: EdgeInsets.only(left: 20.0),
      child: Text(
        comment,
        textAlign: TextAlign.left,
        style: TextStyle(
            fontFamily: "Lato", fontSize: 15.0, color: Color(0xFFa3a5a7)),
      ),
    );
    final userDetails = Column(
      crossAxisAlignment: CrossAxisAlignment.start, // se aline a a la izquierda
      children: <Widget>[userName, userInfo, userComment],
    );

    final photo = Container(
      margin: EdgeInsets.only(top: 20.0, left: 20.0),
      width: 80.0,
      height: 80.0,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            fit: BoxFit.cover, //permte central la imagen
            image: AssetImage(pathImagen),
          )),
    );

    return Row(
      children: <Widget>[
        Flexible(
            child: Card(
          child: Column(
            children: [photo, userDetails],
          ),
        ))
      ],
    );
  }
}
