//importar el paquete de flutter
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_platzi/button_purple.dart';

//las clases los nombres se crean con mayuscula
// ignore: must_be_immutable
class DescriptionPlace extends StatelessWidget {
  String namePlace;
  int stars;
  String descriptionPlace;

  DescriptionPlace(this.namePlace, this.stars, this.descriptionPlace);

  @override
  Widget build(BuildContext context) {
    final starhalf = Container(
        margin: EdgeInsets.only(
          top: 323.0,
          right: 3.0,
        ),
        child: Icon(
          Icons.star_half,
          color: Color(0xFFf2C611),
        ));
    final starborder = Container(
        margin: EdgeInsets.only(
          top: 323.0,
          right: 3.0,
        ),
        child: Icon(
          Icons.star_border,
          color: Color(0xFFf2C611),
        ));
    final star = Container(
        margin: EdgeInsets.only(
          top: 323.0,
          right: 3.0,
        ),
        child: Icon(
          Icons.star,
          color: Color(0xFFf2C611),
        ));
    final titlestars = Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            //permgenerar mejores los margenes
            top: 320.0,
            left: 20.0,
            right: 20.0, //espacios entre los textos
          ),
          child: Text(
            namePlace,
            style: TextStyle(
              fontFamily: "Lato",
              fontSize: 30.0, //tamaño del titulo
              fontWeight: FontWeight.w900, //color negrita
            ),
            textAlign: TextAlign.left,
          ),
        ),
        Row(
          children: <Widget>[star, star, star, starhalf, starborder],
        )
      ],
    );
    final description = Container(
      margin: new EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
      child: new Text(
        descriptionPlace,
        style: const TextStyle(
            fontFamily: "Lato",
            fontSize: 16.0,
            fontWeight: FontWeight.bold,
            color: Color(0xFF56575a)),
      ),
    );
    return Column(
      //para a linear a la izquierda los witgets
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[titlestars, description, ButtonPurple("Navigate")],
    );
  }
}
