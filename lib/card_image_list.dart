import 'package:flutter/material.dart';
import 'package:flutter_platzi/card_image.dart';

class CardImageList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 350.0,
      child: ListView(
        padding: EdgeInsets.all(25.0),
        scrollDirection: Axis.horizontal, //scroll horizontal
        children: <Widget>[
          CardImage("assets/img/recursos/recurso1.jpeg"),
          CardImage("assets/img/recursos/recurso2.jpeg"),
          CardImage("assets/img/recursos/recurso3.jpeg"),
          CardImage("assets/img/recursos/recurso4.jpeg"),
          CardImage("assets/img/recursos/recurso5.jpeg"),
          CardImage("assets/img/recursos/recurso6.jpeg"),
        ],
      ),
    );
  }
}
