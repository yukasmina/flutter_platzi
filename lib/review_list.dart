import 'package:flutter/material.dart';
import 'package:flutter_platzi/review.dart';

class ReviewList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Review("assets/img/user1.jpeg", "Angel Minga", "1 reviews 5 fotos",
            "atención al uso de los widgets Text y TextStyle", 5),
        new Review("assets/img/user2.jpeg", "Mayra", "1 reviews 5 fotos",
            "atención al uso de los widgets Text y TextStyle", 5),
        new Review("assets/img/user3.jpeg", "Juan", "1 reviews 5 fotos",
            "atención al uso de los widgets Text y TextStyle", 5),
      ],
    );
  }
}
