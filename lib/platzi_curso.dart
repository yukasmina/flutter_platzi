import 'package:flutter/material.dart';
import 'package:flutter_platzi/home_trip.dart';
import 'package:flutter_platzi/profile_trips.dart';
import 'package:flutter_platzi/search_trips.dart';

class PlatziCurso extends StatefulWidget {
  @override
  _PlatziCursoState createState() => _PlatziCursoState();
}

class _PlatziCursoState extends State<PlatziCurso> {
  int indexTap = 0; //varaiable para el indice
  /*
     * Lista de item navigator 3
     */
  final List<Widget> widgetChildren = [
    HomeTrip(),
    SearchTrips(),
    ProfileTrips()
  ];
  /*
     * Evento par cambair entre las vistas
     */
  void onTapTapped(int index) {
    setState(() {
      print(index);
      indexTap = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widgetChildren[indexTap],
      bottomNavigationBar: Theme(
          data: Theme.of(context).copyWith(
              canvasColor: Colors.white, //color de la barra
              primaryColor: Colors.purple), //color de iconos
          child: BottomNavigationBar(
            onTap: onTapTapped,
            currentIndex: indexTap,
            items: [
              //creacion de taps
              BottomNavigationBarItem(
                  icon: Icon(Icons.home), title: Text("Inicio")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.search), title: Text("Buscar")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person), title: Text("Perfil")),
            ],
          )),
    );
  }
}
