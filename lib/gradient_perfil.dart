import 'package:flutter/material.dart';

class GradientePerfil extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final gradiente = Container(
      height: 350.0,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [
              //color para el gradiente del + ocuro a + claro
              Color(0xFF5DE68A),
              Color(0xFF1EE3ED)
            ],
            begin: FractionalOffset(
                0.2, 0.0), //orintacion del gradiente entre los colores
            end: FractionalOffset(1.0, 0.6),
            stops: [0.0, 0.6],
            tileMode: TileMode.clamp //color de relleno
            ),
      ),
      //crer un texto
      child: Text(
        "Perfil",
        style: TextStyle(
            color: Colors.white,
            fontSize: 30.0,
            fontFamily: "Lato",
            fontWeight: FontWeight.bold),
      ),
      alignment: Alignment(-0.9, -0.6),
    );

    return gradiente;
  }
}
